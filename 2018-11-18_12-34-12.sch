<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.0">
    <drawing>
        <settings>
            <setting alwaysvectorfont="no"/>
            <setting keepoldvectorfont="yes"/>
            <setting verticaltext="up"/>
        </settings>
        <grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01"
              altunitdist="inch" altunit="inch"/>
        <layers>
            <layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
            <layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
            <layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
            <layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
            <layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
        </layers>
        <schematic>
            <libraries>
                <library name="AcceleratedDesigns_Lib">
                    <packages>
                        <package name="SM_RADIAL_B">
                            <pad name="1" x="-1.6764" y="0" drill="0"/>
                            <pad name="2" x="1.6764" y="0" drill="0"/>
                        </package>
                        <package name="NDP0003B">
                            <pad name="1" x="-4.3" y="2.285" drill="0"/>
                            <pad name="2" x="-4.3" y="-2.285" drill="0"/>
                            <pad name="3" x="2.14" y="0" drill="0"/>
                        </package>
                        <package name="WB_GND">
                            <pad name="1" x="0" y="0" drill="0"/>
                        </package>
                        <package name="WB_BATTERY">
                            <pad name="1" x="0" y="0" drill="0"/>
                            <pad name="2" x="0" y="0" drill="0"/>
                        </package>
                        <package name="WB_CURRENT_LOAD">
                            <pad name="1" x="0" y="0" drill="0"/>
                            <pad name="2" x="0" y="0" drill="0"/>
                        </package>
                    </packages>
                    <symbols>
                        <symbol name="EEE-FK1C100R@1">
                            <pin name="+@1" x="7.62" y="0" visible="off" length="short" direction="pas" rot="MR0"/>
                            <pin name="-@2" x="-7.62" y="0" visible="off" length="short" direction="pas" rot="MR180"/>
                            <wire x1="-1.651184375" y1="-3.047784375" x2="-1.016246875" y2="-2.285859375" width="0.1524"
                                  layer="94" curve="19.54907"/>
                            <wire x1="-1.005578125" y1="-2.292540625" x2="-0.368696875" y2="0.00023125" width="0.1524"
                                  layer="94" curve="33.19223"/>
                            <wire x1="-0.368696875" y1="-0.000228125" x2="-1.005578125" y2="2.29254375" width="0.1524"
                                  layer="94" curve="33.19223"/>
                            <wire x1="-1.016246875" y1="2.285859375" x2="-1.651184375" y2="3.047784375" width="0.1524"
                                  layer="94" curve="19.54907"/>
                            <wire x1="1.016" y1="-3.302" x2="1.016" y2="3.302" width="0.1524" layer="94"/>
                            <wire x1="-7.62" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
                            <wire x1="1.016" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
                            <wire x1="3.556" y1="-2.54" x2="3.556" y2="-1.524" width="0.1524" layer="94"/>
                            <wire x1="3.048" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="94"/>
                            <text x="0.239621875" y="8.047328125" size="2" layer="95" align="center">&gt;NAME</text>
                            <text x="-0.1198125" y="5.291665625" size="2" layer="96" align="center">&gt;VALUE</text>
                        </symbol>
                        <symbol name="WB_GND@1">
                            <pin name="1@1" x="0" y="2.54" visible="off" length="short" direction="pas" rot="MR270"/>
                            <wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-3.302" y1="0" x2="3.302" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="-1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
                            <wire x1="-0.762" y1="-2.032" x2="0.762" y2="-2.032" width="0.1524" layer="94"/>
                        </symbol>
                        <symbol name="LM1117DTX-3.3/NOPB@1">
                            <wire x1="-17.78" y1="-7.62" x2="17.78" y2="-7.62" width="0.2032" layer="94"/>
                            <wire x1="17.78" y1="-7.62" x2="17.78" y2="7.62" width="0.2032" layer="94"/>
                            <wire x1="17.78" y1="7.62" x2="-17.78" y2="7.62" width="0.2032" layer="94"/>
                            <wire x1="-17.78" y1="7.62" x2="-17.78" y2="-7.62" width="0.2032" layer="94"/>
                            <pin name="GND@1" x="22.86" y="-2.54" length="middle" direction="pwr" rot="MR0"/>
                            <pin name="INPUT@2" x="-22.86" y="2.54" length="middle" direction="pwr" rot="MR180"/>
                            <pin name="OUT@3" x="22.86" y="2.54" length="middle" direction="pwr" rot="MR0"/>
                            <text x="0" y="2.54" size="2" layer="95" align="center">&gt;NAME</text>
                            <text x="0" y="0" size="2" layer="96" align="center">&gt;VALUE</text>
                            <text x="0" y="0" size="2" layer="96" align="center">&gt;VALUE</text>
                        </symbol>
                        <symbol name="WB_BATTERY@1">
                            <pin name="+@1" x="-7.62" y="0" visible="off" length="short" direction="pas" rot="MR180"/>
                            <pin name="-@2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="MR0"/>
                            <wire x1="-2.032" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-1.778" y1="-3.048" x2="-1.778" y2="3.048" width="0.1524" layer="94"/>
                            <wire x1="1.016" y1="-3.048" x2="1.016" y2="3.048" width="0.1524" layer="94"/>
                            <wire x1="7.62" y1="0" x2="2.794" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-0.508" y1="-1.27" x2="-0.508" y2="1.27" width="0.1524" layer="94"/>
                            <wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="94"/>
                            <wire x1="-3.556" y1="2.286" x2="-3.556" y2="3.81" width="0.1524" layer="94"/>
                            <wire x1="-2.794" y1="3.048" x2="-4.318" y2="3.048" width="0.1524" layer="94"/>
                            <wire x1="3.302" y1="2.54" x2="3.302" y2="3.81" width="0.1524" layer="94"/>
                            <wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
                            <wire x1="2.286" y1="-1.27" x2="2.286" y2="1.27" width="0.1524" layer="94"/>
                            <text x="0" y="5.08" size="2" layer="95" align="center">&gt;NAME</text>
                        </symbol>
                        <symbol name="WB_CURRENT_LOAD@1">
                            <pin name="+@1" x="-7.62" y="0" visible="off" length="short" direction="pas" rot="MR180"/>
                            <pin name="-@2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="MR0"/>
                            <wire x1="-3.801446875" y1="0.0000625" x2="-2.686315625" y2="-2.6863875" width="0.1524"
                                  layer="94" curve="44.25835"/>
                            <wire x1="-2.700834375" y1="-2.70084375" x2="-0.00003125" y2="-3.821934375" width="0.1524"
                                  layer="94" curve="44.8182"/>
                            <wire x1="-0.000015625" y1="-3.816075" x2="2.6967125" y2="-2.696678125" width="0.1524"
                                  layer="94" curve="45.38076"/>
                            <wire x1="2.69020625" y1="-2.69018125" x2="3.80688125" y2="-0.000015625" width="0.1524"
                                  layer="94" curve="45.58721"/>
                            <wire x1="3.801446875" y1="-0.0000625" x2="2.686315625" y2="2.686384375" width="0.1524"
                                  layer="94" curve="44.25835"/>
                            <wire x1="2.70083125" y1="2.700846875" x2="0.000028125" y2="3.821934375" width="0.1524"
                                  layer="94" curve="44.8182"/>
                            <wire x1="0.000015625" y1="3.816075" x2="-2.6967125" y2="2.696678125" width="0.1524"
                                  layer="94" curve="45.38076"/>
                            <wire x1="-2.69020625" y1="2.69018125" x2="-3.80688125" y2="0.000015625" width="0.1524"
                                  layer="94" curve="45.58721"/>
                            <wire x1="7.62" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-3.81" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
                            <wire x1="0.762" y1="-1.778" x2="3.048" y2="0" width="0.1524" layer="94"/>
                            <wire x1="0.762" y1="1.778" x2="3.048" y2="0" width="0.1524" layer="94"/>
                            <wire x1="3.048" y1="0" x2="-3.048" y2="0" width="0.1524" layer="94"/>
                        </symbol>
                    </symbols>
                    <devicesets>
                        <deviceset name="EEE-FK1C100R">
                            <gates>
                                <gate name="1" symbol="EEE-FK1C100R@1" x="0" y="0" addlevel="always"/>
                            </gates>
                            <devices>
                                <device name="EEE-FK1C100R" package="SM_RADIAL_B">
                                    <connects>
                                        <connect gate="1" pin="+@1" pad="1"/>
                                        <connect gate="1" pin="-@2" pad="2"/>
                                    </connects>
                                    <technologies>
                                        <technology name="">
                                            <attribute name="CAP" value="1.0E-5" constant="no"/>
                                            <attribute name="DATASHEET_URL"
                                                       value="http://www.panasonic.com/industrial/components/pdf/smt%5Faec%5F050505.pdf"
                                                       constant="no"/>
                                            <attribute name="ESR" value="1.35" constant="no"/>
                                            <attribute name="IRMS" value="0.09" constant="no"/>
                                            <attribute name="MANUFACTURER_NAME" value="Panasonic" constant="no"/>
                                            <attribute name="MANUFACTURER_PART_NUMBER" value="EEE-FK1C100R"
                                                       constant="no"/>
                                            <attribute name="VDC" value="16.0" constant="no"/>
                                            <attribute name="VENDOR" value="Panasonic" constant="no"/>
                                        </technology>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                        <deviceset name="WB_GND">
                            <gates>
                                <gate name="1" symbol="WB_GND@1" x="0" y="0" addlevel="always"/>
                            </gates>
                            <devices>
                                <device name="WB_GND" package="WB_GND">
                                    <connects>
                                        <connect gate="1" pin="1@1" pad="1"/>
                                    </connects>
                                    <technologies>
                                        <technology name="">
                                            <attribute name="MANUFACTURER_NAME" value="WB_GND" constant="no"/>
                                            <attribute name="MANUFACTURER_PART_NUMBER" value="WB_GND" constant="no"/>
                                            <attribute name="POWERPORTTYPE" value="POWER_GROUND" constant="no"/>
                                            <attribute name="VALUE" value="Value" constant="no"/>
                                        </technology>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                        <deviceset name="LM1117DTX-3.3/NOPB" prefix="U">
                            <gates>
                                <gate name="1" symbol="LM1117DTX-3.3/NOPB@1" x="0" y="0" addlevel="always"/>
                            </gates>
                            <devices>
                                <device name="LM1117DTX-3.3/NOPB" package="NDP0003B">
                                    <connects>
                                        <connect gate="1" pin="GND@1" pad="1"/>
                                        <connect gate="1" pin="INPUT@2" pad="2"/>
                                        <connect gate="1" pin="OUT@3" pad="3"/>
                                    </connects>
                                    <technologies>
                                        <technology name="">
                                            <attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
                                            <attribute name="MANUFACTURER_NAME" value="Texas Instruments"
                                                       constant="no"/>
                                            <attribute name="MANUFACTURER_PART_NUMBER" value="LM1117DTX-3.3/NOPB"
                                                       constant="no"/>
                                            <attribute name="PARTNUMBER" value="LM1117DTX-3.3/NOPB" constant="no"/>
                                            <attribute name="REFDES" value="RefDes" constant="no"/>
                                            <attribute name="TYPE" value="TYPE" constant="no"/>
                                            <attribute name="VALUE" value="Value" constant="no"/>
                                        </technology>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                        <deviceset name="WB_BATTERY">
                            <gates>
                                <gate name="1" symbol="WB_BATTERY@1" x="0" y="0" addlevel="always"/>
                            </gates>
                            <devices>
                                <device name="WB_BATTERY" package="WB_BATTERY">
                                    <connects>
                                        <connect gate="1" pin="+@1" pad="1"/>
                                        <connect gate="1" pin="-@2" pad="2"/>
                                    </connects>
                                    <technologies>
                                        <technology name="">
                                            <attribute name="MANUFACTURER_NAME" value="WB_BATTERY" constant="no"/>
                                            <attribute name="MANUFACTURER_PART_NUMBER" value="WB_BATTERY"
                                                       constant="no"/>
                                            <attribute name="POWERPORTTYPE" value="POWER_GROUND" constant="no"/>
                                            <attribute name="VALUE" value="Value" constant="no"/>
                                        </technology>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                        <deviceset name="WB_CURRENT_LOAD">
                            <gates>
                                <gate name="1" symbol="WB_CURRENT_LOAD@1" x="0" y="0" addlevel="always"/>
                            </gates>
                            <devices>
                                <device name="WB_CURRENT_LOAD" package="WB_CURRENT_LOAD">
                                    <connects>
                                        <connect gate="1" pin="+@1" pad="1"/>
                                        <connect gate="1" pin="-@2" pad="2"/>
                                    </connects>
                                    <technologies>
                                        <technology name="">
                                            <attribute name="MANUFACTURER_NAME" value="WB_CURRENT_LOAD" constant="no"/>
                                            <attribute name="MANUFACTURER_PART_NUMBER" value="WB_CURRENT_LOAD"
                                                       constant="no"/>
                                            <attribute name="POWERPORTTYPE" value="POWER_GROUND" constant="no"/>
                                            <attribute name="VALUE" value="Value" constant="no"/>
                                        </technology>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                    </devicesets>
                </library>
            </libraries>
            <attributes>
            </attributes>
            <variantdefs>
            </variantdefs>
            <classes>
                <class number="0" name="default" width="0" drill="0">
                </class>
            </classes>
            <parts>
                <part name="CIN" library="AcceleratedDesigns_Lib" deviceset="EEE-FK1C100R" device="EEE-FK1C100R"
                      value="10uF"/>
                <part name="COUT1" library="AcceleratedDesigns_Lib" deviceset="EEE-FK1C100R" device="EEE-FK1C100R"
                      value="10uF"/>
                <part name="GND" library="AcceleratedDesigns_Lib" deviceset="WB_GND" device="WB_GND" value="WB_GND"/>
                <part name="U1" library="AcceleratedDesigns_Lib" deviceset="LM1117DTX-3.3/NOPB"
                      device="LM1117DTX-3.3/NOPB" value="LM1117DTX-3.3/NOPB"/>
                <part name="VIN" library="AcceleratedDesigns_Lib" deviceset="WB_BATTERY" device="WB_BATTERY"
                      value="WB_BATTERY"/>
                <part name="IOUT" library="AcceleratedDesigns_Lib" deviceset="WB_CURRENT_LOAD" device="WB_CURRENT_LOAD"
                      value="WB_CURRENT_LOAD"/>
            </parts>
            <sheets>
                <sheet>
                    <plain>
                    </plain>
                    <instances>
                        <instance part="CIN" gate="1" x="50.8" y="64.77" rot="R90">
                            <attribute name="NAME" x="54.102" y="69.342" size="2" layer="95"/>
                            <attribute name="VALUE" x="54.102" y="64.77" size="2" layer="96"/>
                        </instance>
                        <instance part="COUT1" gate="1" x="149.86" y="66.04" rot="R90">
                            <attribute name="NAME" x="153.162" y="70.612" size="2" layer="95"/>
                            <attribute name="VALUE" x="153.162" y="66.04" size="2" layer="96"/>
                        </instance>
                        <instance part="GND" gate="1" x="95.25" y="27.94">
                            <attribute name="NAME" x="95.25" y="33.528" size="2" layer="95" align="bottom-center"
                                       display="off"/>
                        </instance>
                        <instance part="U1" gate="1" x="88.9" y="87.63">
                            <attribute name="NAME" x="88.9" y="87.63" size="2" layer="95" align="bottom-center"/>
                            <attribute name="TYPE" x="88.9" y="83.82" size="2" layer="96" align="bottom-center"/>
                        </instance>
                        <instance part="VIN" gate="1" x="29.21" y="69.85" rot="R270">
                            <attribute name="NAME" x="33.02" y="74.93" size="2" layer="95"/>
                        </instance>
                        <instance part="IOUT" gate="1" x="173.99" y="54.61" rot="R270">
                            <attribute name="NAME" x="178.054" y="59.944" size="2" layer="95"/>
                        </instance>
                    </instances>
                    <busses>
                    </busses>
                    <nets>
                        <net name="1" class="0">
                            <segment>
                                <pinref part="CIN" gate="1" pin="+@1"/>
                                <pinref part="U1" gate="1" pin="INPUT@2"/>
                                <pinref part="VIN" gate="1" pin="+@1"/>
                                <wire x1="50.8" y1="72.39" x2="50.8" y2="90.17" width="0" layer="91"/>
                                <wire x1="29.21" y1="90.17" x2="66.04" y2="90.17" width="0" layer="91"/>
                                <wire x1="29.21" y1="77.47" x2="29.21" y2="90.17" width="0" layer="91"/>
                            </segment>
                        </net>
                        <net name="0" class="0">
                            <segment>
                                <pinref part="CIN" gate="1" pin="-@2"/>
                                <pinref part="COUT1" gate="1" pin="-@2"/>
                                <pinref part="GND" gate="1" pin="1@1"/>
                                <pinref part="U1" gate="1" pin="GND@1"/>
                                <pinref part="VIN" gate="1" pin="-@2"/>
                                <pinref part="IOUT" gate="1" pin="-@2"/>
                                <wire x1="50.8" y1="34.29" x2="50.8" y2="57.15" width="0" layer="91"/>
                                <wire x1="95.25" y1="34.29" x2="95.25" y2="30.48" width="0" layer="91"/>
                                <wire x1="111.76" y1="85.09" x2="111.76" y2="34.29" width="0" layer="91"/>
                                <wire x1="149.86" y1="58.42" x2="149.86" y2="34.29" width="0" layer="91"/>
                                <wire x1="29.21" y1="34.29" x2="173.99" y2="34.29" width="0" layer="91"/>
                                <wire x1="29.21" y1="34.29" x2="29.21" y2="62.23" width="0" layer="91"/>
                                <wire x1="173.99" y1="34.29" x2="173.99" y2="46.99" width="0" layer="91"/>
                            </segment>
                        </net>
                        <net name="2" class="0">
                            <segment>
                                <pinref part="COUT1" gate="1" pin="+@1"/>
                                <pinref part="U1" gate="1" pin="OUT@3"/>
                                <pinref part="IOUT" gate="1" pin="+@1"/>
                                <wire x1="149.86" y1="73.66" x2="149.86" y2="90.17" width="0" layer="91"/>
                                <wire x1="111.76" y1="90.17" x2="173.99" y2="90.17" width="0" layer="91"/>
                                <wire x1="173.99" y1="62.23" x2="173.99" y2="90.17" width="0" layer="91"/>
                            </segment>
                        </net>
                    </nets>
                </sheet>
            </sheets>
        </schematic>
    </drawing>
</eagle>
