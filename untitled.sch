<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.0">
    <drawing>
        <settings>
            <setting alwaysvectorfont="no"/>
            <setting verticaltext="up"/>
        </settings>
        <grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01"
              altunitdist="inch" altunit="inch"/>
        <layers>
            <layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
            <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
            <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
            <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
            <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
            <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
            <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
            <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
            <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
            <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
            <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
            <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
            <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
            <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
            <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
            <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
            <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
            <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
            <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
            <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
            <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
            <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
            <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
            <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
            <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
            <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
            <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
            <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
            <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
            <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
            <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
            <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
            <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
            <layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
            <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
            <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
            <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
            <layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
            <layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
            <layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
            <layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
            <layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
            <layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
            <layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
            <layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
            <layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
        </layers>
        <schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
            <libraries>
                <library name="transistor-pnp" urn="urn:adsk.eagle:library:399">
                    <description>&lt;b&gt;PNP Transistors&lt;/b&gt;&lt;p&gt;
                        &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;
                    </description>
                    <packages>
                        <package name="SC59-BEC" urn="urn:adsk.eagle:footprint:29187/1" library_version="2">
                            <description>SC59 (SOT23) Motorola</description>
                            <wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
                            <wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
                            <wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
                            <wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
                            <smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
                            <smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
                            <smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
                            <text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
                            <text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
                            <rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
                            <rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
                            <rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
                        </package>
                    </packages>
                    <packages3d>
                        <package3d name="SC59-BEC" urn="urn:adsk.eagle:package:29224/2" type="model"
                                   library_version="2">
                            <description>SC59 (SOT23) Motorola</description>
                            <packageinstances>
                                <packageinstance name="SC59-BEC"/>
                            </packageinstances>
                        </package3d>
                    </packages3d>
                    <symbols>
                        <symbol name="PNP-DRIVER" urn="urn:adsk.eagle:symbol:29188/1" library_version="2">
                            <wire x1="-5.08" y1="0" x2="-4.826" y2="0.762" width="0.1524" layer="94"/>
                            <wire x1="-4.826" y1="0.762" x2="-4.572" y2="-0.508" width="0.1524" layer="94"/>
                            <wire x1="-4.572" y1="-0.508" x2="-4.318" y2="0.762" width="0.1524" layer="94"/>
                            <wire x1="-4.318" y1="0.762" x2="-4.064" y2="-0.508" width="0.1524" layer="94"/>
                            <wire x1="-4.064" y1="-0.508" x2="-3.81" y2="0.762" width="0.1524" layer="94"/>
                            <wire x1="-3.81" y1="0.762" x2="-3.556" y2="-0.508" width="0.1524" layer="94"/>
                            <wire x1="-3.556" y1="-0.508" x2="-3.302" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-3.302" y1="0" x2="-2.286" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="0" x2="-0.254" y2="0" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="0" x2="-2.286" y2="0.762" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="0.762" x2="-2.794" y2="1.016" width="0.1524" layer="94"/>
                            <wire x1="-2.794" y1="1.016" x2="-1.778" y2="1.27" width="0.1524" layer="94"/>
                            <wire x1="-1.778" y1="1.27" x2="-2.794" y2="1.524" width="0.1524" layer="94"/>
                            <wire x1="-2.794" y1="1.524" x2="-1.778" y2="1.778" width="0.1524" layer="94"/>
                            <wire x1="-1.778" y1="1.778" x2="-2.794" y2="2.032" width="0.1524" layer="94"/>
                            <wire x1="-2.794" y1="2.032" x2="-1.778" y2="2.286" width="0.1524" layer="94"/>
                            <wire x1="-1.778" y1="2.286" x2="-2.286" y2="2.54" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="2.54" x2="-2.286" y2="3.556" width="0.1524" layer="94"/>
                            <wire x1="-2.286" y1="3.556" x2="2.54" y2="3.556" width="0.1524" layer="94"/>
                            <wire x1="2.0861" y1="1.6779" x2="1.5781" y2="2.5941" width="0.1524" layer="94"/>
                            <wire x1="1.5781" y1="2.5941" x2="0.5159" y2="1.478" width="0.1524" layer="94"/>
                            <wire x1="0.5159" y1="1.478" x2="2.0861" y2="1.6779" width="0.1524" layer="94"/>
                            <wire x1="2.54" y1="2.54" x2="1.808" y2="2.1239" width="0.1524" layer="94"/>
                            <wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
                            <wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
                            <wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
                            <wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
                            <wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
                            <wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
                            <wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
                            <circle x="-2.286" y="0" radius="0.254" width="0.3048" layer="94"/>
                            <circle x="2.54" y="3.556" radius="0.254" width="0.3048" layer="94"/>
                            <text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
                            <text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
                            <rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
                            <pin name="B" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
                            <pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3"
                                 rot="R90"/>
                            <pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2"
                                 rot="R270"/>
                        </symbol>
                    </symbols>
                    <devicesets>
                        <deviceset name="MUN2113*" urn="urn:adsk.eagle:component:29354/2" prefix="Q"
                                   library_version="2">
                            <description>&lt;b&gt;PNP Bias Resistor Transistor&lt;/b&gt;</description>
                            <gates>
                                <gate name="G$1" symbol="PNP-DRIVER" x="0" y="0"/>
                            </gates>
                            <devices>
                                <device name="" package="SC59-BEC">
                                    <connects>
                                        <connect gate="G$1" pin="B" pad="B"/>
                                        <connect gate="G$1" pin="C" pad="C"/>
                                        <connect gate="G$1" pin="E" pad="E"/>
                                    </connects>
                                    <package3dinstances>
                                        <package3dinstance package3d_urn="urn:adsk.eagle:package:29224/2"/>
                                    </package3dinstances>
                                    <technologies>
                                        <technology name="T1"/>
                                    </technologies>
                                </device>
                            </devices>
                        </deviceset>
                    </devicesets>
                </library>
            </libraries>
            <attributes>
            </attributes>
            <variantdefs>
            </variantdefs>
            <classes>
                <class number="0" name="default" width="0" drill="0">
                </class>
            </classes>
            <modules>
                <module name="TEST" prefix="" dx="30.48" dy="20.32">
                    <ports>
                    </ports>
                    <variantdefs>
                    </variantdefs>
                    <parts>
                        <part name="Q1" library="transistor-pnp" library_urn="urn:adsk.eagle:library:399"
                              deviceset="MUN2113*" device="" package3d_urn="urn:adsk.eagle:package:29224/2"
                              technology="T1"/>
                    </parts>
                    <sheets>
                        <sheet>
                            <plain>
                            </plain>
                            <instances>
                                <instance part="Q1" gate="G$1" x="48.26" y="48.26">
                                    <attribute name="NAME" x="38.1" y="55.88" size="1.778" layer="95"/>
                                    <attribute name="VALUE" x="38.1" y="53.34" size="1.778" layer="96"/>
                                </instance>
                            </instances>
                            <busses>
                            </busses>
                            <nets>
                            </nets>
                        </sheet>
                    </sheets>
                </module>
            </modules>
            <parts>
            </parts>
            <sheets>
                <sheet>
                    <plain>
                    </plain>
                    <moduleinsts>
                        <moduleinst name="TEST1" module="TEST" x="27.94" y="71.12">
                            <attribute name="NAME" x="27.94" y="71.12" size="2.032" layer="95" align="bottom-center"/>
                        </moduleinst>
                        <moduleinst name="TEST2" module="TEST" x="63.5" y="30.48">
                            <attribute name="NAME" x="63.5" y="30.48" size="2.032" layer="95" align="bottom-center"/>
                        </moduleinst>
                        <moduleinst name="TEST3" module="TEST" x="99.06" y="73.66">
                            <attribute name="NAME" x="99.06" y="73.66" size="2.032" layer="95" align="bottom-center"/>
                        </moduleinst>
                    </moduleinsts>
                    <instances>
                    </instances>
                    <busses>
                    </busses>
                    <nets>
                    </nets>
                </sheet>
            </sheets>
        </schematic>
    </drawing>
    <compatibility>
        <note version="8.2" severity="warning">
            Since Version 8.2, EAGLE supports online libraries. The ids
            of those online libraries will not be understood (or retained)
            with this version.
        </note>
        <note version="8.3" severity="warning">
            Since Version 8.3, EAGLE supports URNs for individual library
            assets (packages, symbols, and devices). The URNs of those assets
            will not be understood (or retained) with this version.
        </note>
        <note version="8.3" severity="warning">
            Since Version 8.3, EAGLE supports the association of 3D packages
            with devices in libraries, schematics, and board files. Those 3D
            packages will not be understood (or retained) with this version.
        </note>
    </compatibility>
</eagle>
